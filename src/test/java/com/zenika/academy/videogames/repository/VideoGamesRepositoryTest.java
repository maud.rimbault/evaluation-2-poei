package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VideoGamesRepositoryTest {
    private VideoGamesRepository repo;

    @BeforeEach
    void setUp() {
        repo = new VideoGamesRepository();
    }

    @Test
    @DisplayName("Renvoie vide si le repo est vide")
    void emptyRepo() {
        assertEquals(Optional.empty(), repo.get().isEmpty());

    }


}
